if (/y\.music\.163\.com\/login/.test(location.href)) {
    Object.defineProperty(
        window.navigator,
        'userAgent',
        { value: 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1' }
    )

    fetch('/m/login')
        .then(_ => _.text())
        .then(body => {
            body = body.replace('window.location.replace', '')
            document.body.innerHTML = body
            history.replaceState(history.state, null, '/m/login')
            const scripts = document.querySelectorAll('script')
            scripts.forEach(script => {
                const { innerHTML, src } = script
                if (innerHTML) eval(innerHTML)
                if (src) {
                    const reload = document.createElement('script')
                    reload.src = src
                    script.parentNode.replaceChild(reload, script)
                }
            })
        })
}