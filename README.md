# AccessNeteaseMusicMobile

PC 端直接访问网易云音乐移动端页面

- 修改请求头 `User-Agent` (防止服务端重定向)

- 修改页面内 `window.navigator.userAgent` (防止前端重定向)
