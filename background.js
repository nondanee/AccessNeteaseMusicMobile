const UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'

chrome.webRequest.onBeforeSendHeaders.addListener(
    details => {
        const { requestHeaders } = details
        const index = requestHeaders.findIndex(({ name }) => /^User-Agent$/i.test(name))
        if (index !== -1) requestHeaders.splice(index, 1)
        requestHeaders.push({ name: 'User-Agent', value: UA })
        return { requestHeaders }
    },
    {
        urls: ['*://*.music.163.com/m/*'],
    },
    ['blocking', 'requestHeaders']
)
